package org.example.micronaout;

import io.micronaut.runtime.Micronaut;

public class Application {
  public static void main(String... arg) {
    Micronaut.run(Application.class);
  }
}
