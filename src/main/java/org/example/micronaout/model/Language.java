package org.example.micronaout.model;

public enum Language {
  DE("de");
  private final String dictionary;

  Language(String dictionary) {
    this.dictionary = dictionary;
  }

  public String getDictionary() {
    return dictionary;
  }
}
