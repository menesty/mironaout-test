package org.example.micronaout.controller;


import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.views.View;
import org.example.micronaout.model.Language;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Controller
public class HomeController {

  @Secured(SecurityRule.IS_ANONYMOUS)
  @View("home")
  @Get
  public Map<String, Object> index(@Nullable Language lang) {
    Objects.requireNonNull(lang);
    return new HashMap<>();
  }
}