package org.example.micronaout.filter;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Filter;
import io.micronaut.http.filter.HttpServerFilter;
import io.micronaut.http.filter.ServerFilterChain;
import io.reactivex.Flowable;
import org.example.micronaout.model.Language;
import org.reactivestreams.Publisher;

@Filter("/**")
public class LanguageHttpServerFilter implements HttpServerFilter {
  public static final String LANGUAGE_KEY = "lang";

  @Override
  public Publisher<MutableHttpResponse<?>> doFilter(HttpRequest<?> request, ServerFilterChain chain) {
    return Flowable.fromCallable(() -> {
      request.getAttributes().put(LANGUAGE_KEY, Language.DE);

      return true;
    }).switchMap(aBoolean -> chain.proceed(request));
  }
}
